NodeJS server for Freight Delivery web application<br />

To start testing you need to download server repository and in terminal:
- npm install - to install all node_modules;
- npm start - to run server("node index.js").<br />

UI for testing this server - https://lingsat.github.io/Freight-Delivery-UI-for-deploy/

React UI project on gitHub - https://github.com/lingsat/Freight-Delivery-front-end

Author: Serhii Petrenko<br />
Email: lingsatpilingast@gmail.com